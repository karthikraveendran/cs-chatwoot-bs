//insentLoadLazy = true; //optional parameter to hide wigdet until setVisitor is called.
insentCompanyDomain = 'insent.ai';
insentProjectName = 'livedemo'
insentProjectKey = 'w6XubFQg5Dwt2wacK8EU'
var t = window.insent || {};
t.queue = [];
(t.SCRIPT_VERSION = '0.1.2'),
    (t.methods = ['widget', 'listener', 'setVisitor']),
    (t.factory = function (e) {
        return function () {
            var n = Array.prototype.slice.call(arguments);
            return n.unshift(e), t.queue.push(n), t;
        };
    }),
    t.methods.forEach(function (e) {
        t[e] = t.factory(e);
    });
insent = t;
var s = document.createElement('script');
(s.type = 'text/javascript'), (s.charset = 'utf-8'), (s.defer = !0),
    (s.src = 'https://livedemo.widget.insent.ai/insent'),
    window.addEventListener('load', function (n) {
        document.body.appendChild(s);
    });