export default function Patients() {
    return (
        <section className="patients" id="patients">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6">
                        <h3 className="title-40 text-700 text-blue">New Patients</h3>
                        <p>From the moment you step into our dental office, our team goes all-out to help you feel more
                            comfortable. Even on the first visit, we like to greet our patients by name and make sure
                            each of their personal needs are acknowledged.</p>
                        <p>During your visit, we’ll listen to your health concerns and goals, answer any questions, and
                            explain all your options. You’ll love our friendly, relaxing environment, personable
                            approach, and high-tech, minimally invasive techniques.</p>

                    </div>

                    <div className="col-lg-6">
                        <div className="patients__content">
                            <h4 className="patients__title title-22 text-700">Looking for something?</h4>

                            <div className="bullet-item">
                                <p>COVID safety norms hindering your physical visit?</p>
                                <a href="#" className="link-blue-underlined" target="_blank">Take a Virtual
                                    Appointment</a>
                            </div>

                            <div className="bullet-item">
                                <p>Planning for a visit?</p>
                                <a href="https://brightsmiles.patient.demo.carestack.com/#/appointment-booking"
                                   className="link-blue-underlined" target="_blank" rel="noreferrer">Schedule Your First Visit</a>
                            </div>


                            <div className="bullet-item">
                                <p>Need to know more about insurance & financing plans?</p>
                                <a href="#resources" className="link-blue-underlined">Check Our Resources </a>
                            </div>


                            <div className="bullet-item border-bottom-0">
                                <p>Something else need to talk?</p>
                                Call: <a href="tel:(877) 0000-0000" className="link-blue-underlined">(877)
                                0000-0000</a> or email: <a href="mailto:smile@brightsmiles.com"
                                                           className="link-blue-underlined">smile@brightsmiles.com</a>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>
    );
}