export default function Testimonials() {
    return (

        <section className="testimonials" id="testimonials">
            <div className="section-header text-center">
                <h4 className="title-40 text-700 text-blue">Patient Stories</h4>
                <p> We have professionalized the art of bringing back smiles and it seems our patients can’t stop talking about it.</p>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-md-6">
                        <div className="testimonial-box">
                            <img src="images/abstract/quote.svg" alt="Quote" className="quote-icon"/>
                                <blockquote className="testimonial-box__quote">Even though I can feel anxious about going to the dentist, Bright Smiles staff makes it enjoyable and more comfortable.</blockquote>

                                <figure className="testimonial-box__headshot">
                                    <img src="images/headshots/1.png" alt=""/>
                                </figure>
                                <h6 className="testimonial-box__name">James Snow</h6>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6">
                        <div className="testimonial-box">
                            <img src="images/abstract/quote.svg" alt="Quote" className="quote-icon"/>
                                <blockquote className="testimonial-box__quote">The entire team has made me comfortable about going to the dentist again. What you do for your patients is inspiring. </blockquote>

                                <figure className="testimonial-box__headshot">
                                    <img src="images/headshots/2.png" alt=""/>
                                </figure>
                                <h6 className="testimonial-box__name">Jordan Jefferson</h6>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6">
                        <div className="testimonial-box">
                            <img src="images/abstract/quote.svg" alt="Quote" className="quote-icon"/>
                                <blockquote className="testimonial-box__quote">Fantastic team and enriching experience. Clean, efficient and wonderful staff. Made me feel right at home.</blockquote>

                                <figure className="testimonial-box__headshot">
                                    <img src="images/headshots/3.png" alt=""/>
                                </figure>
                                <h6 className="testimonial-box__name">Alicia Moniker</h6>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}