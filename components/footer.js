export default function Footer() {
    return (
        <footer id="siteFooter">
            <div className="footer-widget-area">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-6 logo-col">
                            <figure className="footer-logo">
                                <img src="images/logos/bright-smiles-logo.svg" alt="Bright Smiles Logo"/>
                            </figure>

                            <p className="footer-about">
                                At Bright Smiles we are ready to exceed your expectations. To learn more about our
                                comprehensive dental services, contact our office today.
                            </p>
                        </div>

                        <div className="col-lg col-md-6">
                            <div className="widget">
                                <h4 className="widget__title">CONTACT US</h4>

                                <a href="tel:818-0000-0000" className="d-block link-blue text-22 contact-link">(818)
                                    0000-0000</a>
                                <a href="mailto:smile@brightsmiles.com"
                                   className="d-block link-blue text-16 email-link">smile@brightsmiles.com</a>

                                <div className="address text-16">
                                    954 Mallory Circle, Suite 209, <br></br>Celebration, FL 34747
                                </div>
                            </div>
                        </div>

                        <div className="col-lg col-md-6">
                            <div className="widget">
                                <h4 className="widget__title">OFFICE HOURS</h4>

                                <div className="office-hours text-16">
                                    Mon – Thu : 8:00 am – 5:00 pm <br></br>
                                    Friday – Sunday: Closed
                                </div>
                            </div>

                            <div className="widget">
                                <div className="widget__social">
                                    Follow us:
                                    <a href="#"><i className="fa fa-facebook"></i></a>
                                    <a href="#"><i className="fa fa-twitter"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="footer-copyright-area">
                <div className="container">
                    <div className="text-center">
                        <p>Copyright © 2020 <a href="#home">Bright Smiles</a> <b>|</b> All Rights Reserved <b>|</b> <a
                            href="#">Privacy Policy</a> <b>|</b> <a href="#">Terms of Use</a> <b>|</b> <a
                            href="#">Disclaimer</a> <b>|</b> Web Accessibility Statement </p>
                    </div>
                </div>
            </div>
        </footer>
    );
}