

export default function Header({children}) {
    const scroll = (id) => {
        debugger;
        const section = document.getElementById( id );
        section.scrollIntoView( { behavior: 'smooth', block: 'center' } );
    };
    return (
        <>
            <header id="siteHeader" className="site-header">
                <div className="topbar">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-4">
                                <div className="topbar__stuff">
                                    <span className="topbar__info">Call: (877) 545-4188</span>
                                    <span className="topbar__info">Mon-Fri : 8:00am - 6:00pm CST</span>
                                </div>

                            </div>

                            <div className="col-sm-8 text-right">
                                <div className="topbar__social">
                                    <a href="https://www.facebook.com/CareStackSystem/"><i className="fa fa-facebook"></i></a>
                                    <a href="https://twitter.com/CareStackSystem/"><i className="fa fa-twitter"></i></a>
                                </div>

                                <div className="topbar__links">
                                    <a href="#testimonials" className="link-bg-white">Reviews</a>
                                    <a href="#siteFooter" className="link-white">Contact Us</a>
                                </div>

                                <div className="topbar__login">
                                    <a href="https://patientportal.demo.carestack.com/?account=10067"
                                       className="link-white" target="_blank" rel="noreferrer">
                                        <img src="images/abstract/login.svg" alt=""/>
                                        <span>Patient Login</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <nav className="navbar navbar-expand-lg navbar-bright bg-white">
                    <div className="container">
                        <a className="navbar-brand" href="#">
                            <img src="images/logos/bright-smiles-logo.svg" alt="Bright Smiles Logo"/>
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#brightNav"
                                aria-controls="brightNav" aria-expanded="false" aria-label="Toggle navigation">
                              <span className="navbar-toggler-icon">
                                <i className="fa fa-bars"></i>
                              </span>
                        </button>

                        <div className="collapse navbar-collapse" id="brightNav">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <a className="nav-link" onClick={() => {scroll('about')}}>About Us</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" onClick={() => {scroll('services')}}>Services</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" onClick={() => {scroll('team')}}>Team</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" onClick={() => {scroll('patients')}}>New Patients</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" onClick={() => {scroll('resources')}}>Resources</a>
                                </li>
                            </ul>

                            <a href="https://patientportal.demo.carestack.com/?dn=brightsmiles/#/online-appointments"
                               className="btn btn-green" target="_blank" rel="noreferrer">Book Now</a>
                        </div>
                    </div>
                </nav>
            </header>
        </>
    )
}