export default function Team() {
    return (
        <section className="team" id="team">
            <div className="container">
                <div className="section-header text-center mb-50">
                    <h3 className="title-40 text-blue text-700 mb-30">A Top Rated Clinical Team</h3>
                    <p className="section-text mx-auto">Improving the overall health of our patients is our main goal.
                        Each doctor at Bright Smiles offer expert knowledge in their specialized goal giving you the
                        best care possible.</p>
                </div>

                <div className="row">
                    <div className="col-sm-6 col-md-4 col-lg">
                        <div className="member">
                            <figure className="member__image">
                                <img src="images/headshots/amy.png" alt="" className="w-100"/>
                            </figure>

                            <h6 className="member__name">Amy Williford, DMD</h6>
                            <p className="member__designation">Oral Surgeon</p>
                        </div>

                    </div>


                    <div className="col-sm-6 col-md-4 col-lg">
                        <div className="member">
                            <figure className="member__image">
                                <img src="images/headshots/cody.png" alt="" className="w-100"/>
                            </figure>

                            <h6 className="member__name">Cody Mast, DDS</h6>
                            <p className="member__designation">General Dentist</p>
                        </div>

                    </div>


                    <div className="col-sm-6 col-md-4 col-lg">
                        <div className="member">
                            <figure className="member__image">
                                <img src="images/headshots/david.png" alt="" className="w-100"/>
                            </figure>

                            <h6 className="member__name">David Jones , III, DMD</h6>
                            <p className="member__designation">Periodontist</p>
                        </div>

                    </div>


                    <div className="col-sm-6 col-md-4 col-lg">
                        <div className="member">
                            <figure className="member__image">
                                <img src="images/headshots/tami.png" alt="" className="w-100"/>
                            </figure>

                            <h6 className="member__name">Tami Rushing, DMD</h6>
                            <p className="member__designation">General Dentist</p>
                        </div>

                    </div>


                    <div className="col-sm-6 col-md-4 col-lg">
                        <div className="member">
                            <figure className="member__image">
                                <img src="images/headshots/william.png" alt="" className="w-100"/>
                            </figure>

                            <h6 className="member__name">William Joseph, DDS</h6>
                            <p className="member__designation">General Dentist</p>
                        </div>

                    </div>

                </div>

            </div>

        </section>
    );
}