export default function Services() {
    return(
        <section className="services" id="services">
            <div className="container">

                <div className="featured">
                    <div className="featured__title">
                        <span className="f-line"></span>
                        <span className="f-text">FEATURED SERVICE</span>
                        <span className="f-line"></span>
                    </div>
                </div>

                <figure className="featured__image">
                    <img src="images/colgate.png" alt="" className="w-100"/>
                </figure>

                <div className="featured__content">
                    <h3 className="title-40 text-700 text-black"><span className="text-colgate">Now offering Optic White Professional,</span> ask our dental professionals during your visit</h3>
                    <p className="featured__text">Achieve whiter & brighter teeth with Colgate&apos;s new Optic White Professional system, available at our office. Quickly achieve a beautiful white smile and lighten any discolorations on your teeth.</p>

                    <a href="#" className="btn btn-colgate">Learn More</a>
                </div>

                <div className="section-header text-center mb-50">
                    <h3 className="title-40 text-700 text-black mb-20">Our Clinical Services</h3>
                    <p className="section-text mx-auto"> We’re proud to offer high-tech, high-touch dental care that will maximize your comfort and provide minimally invasive treatments with amazing results. Take a detailed look at our services below. </p>
                </div>

                <div className="services">
                    <div className="row">
                        <div className="col-lg-4 col-md-6">
                            <div className="image-box">
                                <figure className="image-box__image img-1">
                                    <figcaption>Professional Cleaning</figcaption>
                                </figure>

                                <div className="image-box__content">
                                    <p className="image-box__text">Our hygienist will guide you through the processes and determine the one best suited to your needs.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-6">
                            <div className="image-box">
                                <figure className="image-box__image img-2">
                                    <figcaption>General Dentistry</figcaption>
                                </figure>

                                <div className="image-box__content">
                                    <p className="image-box__text">We offer a wide array of general dental services and procedures like fillings, crowns, bridges etc.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-6">
                            <div className="image-box">
                                <figure className="image-box__image img-3">
                                    <figcaption>Cosmetic Dentistry</figcaption>
                                </figure>

                                <div className="image-box__content">
                                    <p className="image-box__text">Our cosmetic dentists specialize in creating a treatment plan just for you to help you achieve the smile of your dreams.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-6">
                            <div className="image-box">
                                <figure className="image-box__image img-4">
                                    <figcaption>Digital X-Rays</figcaption>
                                </figure>

                                <div className="image-box__content">
                                    <p className="image-box__text">With the latest technology, X-rays can now be viewed and enhanced at the dental chair for better diagnosis.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-6">
                            <div className="image-box">
                                <figure className="image-box__image img-5">
                                    <figcaption>Prosthodontics</figcaption>
                                </figure>

                                <div className="image-box__content">
                                    <p className="image-box__text">We offer several treatments in dental implants, crowns, bridges, dentures and temporomandibular disorders.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-6">
                            <div className="image-box">
                                <figure className="image-box__image img-6">
                                    <figcaption>Oral Surgery</figcaption>
                                </figure>

                                <div className="image-box__content">
                                    <p className="image-box__text"> Surgical procedures for wisdom teeth, tooth loss, dental implants, dentofacial deformities, sinus lift etc. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}