export default function Hero() {
    return (
        <section className="hero" id="home">
            <div className="container">
                <div className="hero__inner">
                    <div className="hero__content">
                        <h1 className="hero__title">Comprehensive & Personalized Dental Care</h1>
                        <p className="hero__text">Re-imagining the dental experience <br></br>and setting a new standard for
                            patient care</p>

                        <a href="#services" className="btn btn-green">View Our Services</a>
                    </div>
                </div>
            </div>
        </section>
    );
}