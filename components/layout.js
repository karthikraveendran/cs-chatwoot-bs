import Head from 'next/head';
import Header from "./header";
import Script from 'next/script'
import Footer from "./footer";
import {Fragment} from "react";

export default function Layout({ children }) {
    return (
        <Fragment>
            <Head>
                <meta charSet="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
                <link rel="shortcut icon" href="images/favicon/favicon.png" />
                <title>Bright Smiles</title>
            </Head>
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet"/>
            <Header />
            <Script src="https://kit.fontawesome.com/7ab1dfc34f.js"></Script>
            <main>{children}</main>
            <Script src="js/chatwoot.js"></Script>
            <Footer></Footer>
        </Fragment>
    )
}