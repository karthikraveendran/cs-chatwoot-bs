export default function Features() {
    return(
        <section className="oralhealth">
            <div className="container">

                <div className="features">
                    <div className="row">
                        <div className="col-md-6 col-lg-4 col-xl-3">
                            <div className="icon-box">
                                <figure className="icon-box__icon">
                                    <img src="images/icons/1.svg" alt=""/>
                                </figure>
                                <h4 className="icon-box__title title-22">Safety first</h4>
                                <p className="icon-box__text text-16">
                                    Regular health checks, sanitization and provision of PPE, etc.
                                </p>
                            </div>
                        </div>

                        <div className="col-md-6 col-lg-4 col-xl-3">
                            <div className="icon-box">
                                <figure className="icon-box__icon">
                                    <img src="images/icons/2.svg" alt=""/>
                                </figure>
                                <h4 className="icon-box__title title-22">Insurance accepted</h4>
                                <p className="icon-box__text text-16">
                                    We accept major insurers like Delta Dental, Aetna, and Cigna.
                                </p>
                            </div>
                        </div>

                        <div className="col-md-6 col-lg-4 col-xl-3">
                            <div className="icon-box">
                                <figure className="icon-box__icon">
                                    <img src="images/icons/3.svg" alt=""/>
                                </figure>
                                <h4 className="icon-box__title title-22">Membership Plans</h4>
                                <p className="icon-box__text text-16">
                                    For uninsured patients, we offer in-house membership plans.
                                </p>
                            </div>
                        </div>

                        <div className="col-md-6 col-lg-4 col-xl-3">
                            <div className="icon-box">
                                <figure className="icon-box__icon">
                                    <img src="images/icons/4.svg" alt=""/>
                                </figure>
                                <h4 className="icon-box__title title-22">Curbside Check-in</h4>
                                <p className="icon-box__text text-16">
                                    No more waiting in lobbies or common areas.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}