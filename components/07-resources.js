export default function Resources() {
    return (
        <section className="resources" id="resources">
            <div className="container">
                <div className="section-header text-center">
                    <h3 className="title-40 text-700 text-blue">Resources</h3>
                    <p>Thank you for choosing Bright Smiles. We have a list of resources for you to utilize and learn
                        about the services that we can offer you. Click the icons below to access our resources.</p>
                </div>
                

                <div className="resources">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-lg-4 col-md-6">
                                <div className="r-item r-lined">
                                    <figure className="r-item__icon">
                                        <img src="images/resources/1.svg" alt=""/>
                                    </figure>
                                    <h3 className="r-item__title">Insurance & Finance </h3>
                                    <p className="r-item__text">Learn about insurance and forms of payment we
                                        accept.</p>
                                </div>
                                
                            </div>
                            

                            <div className="col-lg-4 col-md-6">
                                <div className="r-item r-lined">
                                    <figure className="r-item__icon">
                                        <img src="images/resources/1.svg" alt=""/>
                                    </figure>
                                    <h3 className="r-item__title">Policies</h3>
                                    <p className="r-item__text">Learn more about our Privacy and Security Policy</p>
                                </div>
                                
                            </div>
                            

                            <div className="col-lg-4 col-md-6">
                                <div className="r-item">
                                    <figure className="r-item__icon">
                                        <img src="images/resources/1.svg" alt=""/>
                                    </figure>
                                    <h3 className="r-item__title">Forms</h3>
                                    <p className="r-item__text">View new patient & orthodontic treatment forms</p>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
            
        </section>
    );
}