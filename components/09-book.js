export default function Book() {
    return (
        <section className="book">
            <div className="container">
                <div className="row">
                    <div className="col-xl-8 text-center">
                        <p className="book__text text-white">Care at Bright Smiles isn’t just painless, it’s a
                            pleasure.</p>
                    </div>
                    

                    <div className="col-xl-4">
                        <div className="btn-wrapper">
                            <a href="https://patientportal.demo.carestack.com/?dn=brightsmiles/#/online-appointments"
                               className="btn btn-green" target="_blank" rel="noreferrer">Book Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}