export default function DentalCare() {
    return(
        <section className="studio" id="about">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6">
                        <div className="studio__content">
                            <div className="section-header">
                                <h3 className="title-40 text-700 text-blue mb-20">Convenient and <span>Modern Dental Care</span>
                                </h3>
                            </div>

                            <p className="mb-20">Bright Smiles Dental serves to provide our industry-best dental care
                                without delay. Utilizing cutting-edge technologies like Teledentistry and Curbside
                                Check-in, we ensure that all your needs are addressed without putting our patient’s
                                personal safety at risk.</p>


                            <a href="https://patientportal.demo.carestack.com/?dn=brightsmiles/#/online-appointments"
                               className="btn btn-green" target="_blank" rel="noreferrer">Book Now</a>
                        </div>

                    </div>

                    <div className="col-lg-6">
                        <div className="studio-gallery">
                            <div className="grid grid-1">
                                <figure className="std-image__l">
                                    <img src="images/featured/g1.png" alt="" className="w-100"/>
                                </figure>
                            </div>
                            <div className="grid grid-2">
                                <figure className="std-image__r">
                                    <img src="images/featured/g2.png" alt="" className="w-100"/>
                                </figure>

                                <figure className="std-image__r">
                                    <img src="images/featured/g3.png" alt="" className="w-100"/>
                                </figure>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    );
}