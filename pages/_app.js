import Layout from '../components/layout'
import styles from '../styles/globals.css'

export default function BrightSmilesApp({ Component, pageProps }) {
  return (
      <Layout>
        <Component {...pageProps} />
      </Layout>
  )
}