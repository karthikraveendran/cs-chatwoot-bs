import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Hero from "../components/01-hero";
import DentalCare from "../components/02-dentalcare";
import Features from "../components/03-features";
import Services from "../components/04-services";
import Team from "../components/05-team";
import Patients from "../components/06-patients";
import Resources from "../components/07-resources";
import Testimonials from "../components/08-testimonials";
import Book from "../components/09-book";

export default function Home() {
    return (
        <>
            <Hero></Hero>
            <DentalCare></DentalCare>
            <Features></Features>
            <Services></Services>
            <Team></Team>
            <Patients></Patients>
            <Resources></Resources>
            <Testimonials></Testimonials>
            <Book></Book>
        </>
    )
}
